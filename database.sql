-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 14, 2018 at 10:11 AM
-- Server version: 5.6.31-0ubuntu0.15.10.1
-- PHP Version: 5.6.11-1ubuntu3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web_scraping`
--

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id` int(11) NOT NULL,
  `nik` int(11) NOT NULL,
  `nama` varchar(32) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `nik`, `nama`) VALUES
(2, 70100, 'kamsir'),
(20, 123, '123'),
(21, 123123, 'sadf'),
(22, 666, 'Anto');

-- --------------------------------------------------------

--
-- Table structure for table `scraping`
--

CREATE TABLE IF NOT EXISTS `scraping` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `product` varchar(12) NOT NULL,
  `note` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scraping`
--

INSERT INTO `scraping` (`id`, `users_id`, `product`, `note`, `create_date`) VALUES
(1, 5, 'cjr.catenzo.', '', '2018-01-18 09:23:21'),
(2, 5, 'cjr.catenz', 'asdfdsa', '2018-01-18 09:43:14'),
(3, 5, 'cjr.catenz', 'Tolong di bantu', '2018-01-18 09:45:01'),
(4, 5, 'cjr.catenz', 'Tolong di bantu', '2018-01-18 09:46:35'),
(5, 5, 'cjr.catenz', 'Tolong di bantu', '2018-01-18 09:47:12'),
(6, 5, 'cjr.catenz', 'Tolong di bantu', '2018-01-18 09:48:22'),
(7, 5, 'ctnz.catenzo', 'WOWOWO', '2018-01-18 09:59:34');

-- --------------------------------------------------------

--
-- Table structure for table `table_keterangan`
--

CREATE TABLE IF NOT EXISTS `table_keterangan` (
  `id` int(11) NOT NULL,
  `no` int(11) NOT NULL,
  `kode` varchar(30) NOT NULL,
  `warna` varchar(30) NOT NULL,
  `berat` varchar(30) NOT NULL,
  `bahan` varchar(30) NOT NULL,
  `info` text NOT NULL,
  `scraping_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=363 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_keterangan`
--

INSERT INTO `table_keterangan` (`id`, `no`, `kode`, `warna`, `berat`, `bahan`, `info`, `scraping_id`) VALUES
(313, 361, 'CIN 030', 'PINK KOMBINASI', 'CANVAS', '450 gr', '28 X 43 X 13', 5),
(314, 362, 'CAI 025', 'PINK KOMBINASI', 'DOLBY', '500 gr', '32 X 12 X 42', 5),
(315, 363, 'CCL 001', 'COKLAT', 'DINIR', '480 gr', '28 X 42 X 14', 5),
(316, 364, 'CCL 002', 'ABU ABU', 'DINIR', '430 gr', '30 X 45 X 16', 5),
(317, 365, 'CYD 276', 'MERAH KOMBINASI', 'DINIR', '300 gr', '27 X 12 X 37', 5),
(318, 366, 'CST 007', 'HITAM', 'DOLBY', '500 gr', '25 X 40 X 12, + RAIN COVER', 5),
(319, 367, 'CYD 275', 'BIRU', 'DOLBY', '300 gr', '27 X 12 X 40', 5),
(320, 368, 'CST 274', 'HITAM', 'CORDURA', '400 gr', '29 X 12 X 42', 5),
(321, 369, 'CRZ 189', 'TAN', 'DINIR', '500 gr', '31 X 17 X 43', 5),
(322, 370, 'CST 006', 'MERAH', 'CANVAS', '500 gr', '28 X 40 X 12, + RAIN COVER', 5),
(323, 361, 'CIN 030', 'PINK KOMBINASI', 'CANVAS', '450 gr', '28 X 43 X 13', 6),
(324, 362, 'CAI 025', 'PINK KOMBINASI', 'DOLBY', '500 gr', '32 X 12 X 42', 6),
(325, 363, 'CCL 001', 'COKLAT', 'DINIR', '480 gr', '28 X 42 X 14', 6),
(326, 364, 'CCL 002', 'ABU ABU', 'DINIR', '430 gr', '30 X 45 X 16', 6),
(327, 365, 'CYD 276', 'MERAH KOMBINASI', 'DINIR', '300 gr', '27 X 12 X 37', 6),
(328, 366, 'CST 007', 'HITAM', 'DOLBY', '500 gr', '25 X 40 X 12, + RAIN COVER', 6),
(329, 367, 'CYD 275', 'BIRU', 'DOLBY', '300 gr', '27 X 12 X 40', 6),
(330, 368, 'CST 274', 'HITAM', 'CORDURA', '400 gr', '29 X 12 X 42', 6),
(331, 369, 'CRZ 189', 'TAN', 'DINIR', '500 gr', '31 X 17 X 43', 6),
(332, 370, 'CST 006', 'MERAH', 'CANVAS', '500 gr', '28 X 40 X 12, + RAIN COVER', 6),
(333, 371, 'CTP 270', 'MERAH', 'CANVAS', '300 gr', '38 X 9 X 36', 6),
(334, 372, 'CYD 277', 'PINK', 'DINIR', '300 gr', '27 X 12 X 37', 6),
(335, 373, 'CTP 268', 'PUTIH', 'CANVAS', '300 gr', '32 X 38 X 10', 6),
(336, 374, 'CTP 271', 'HITAM', 'DINIR', '200 gr', '22 X 9 X 18', 6),
(337, 375, 'CBD 174', 'HITAM', 'SINTETIS', '440 gr', '29 X 38 X 17', 6),
(338, 376, 'CBD 171', 'COKLAT', 'DINIR', '430 gr', '18 X 39 X 11', 6),
(339, 377, 'CCL 011', 'ABU ABU', 'DINIR', '300 gr', '23 X 10 X 33', 6),
(340, 378, 'CAI 026', 'PINK', 'DOLBY', '0 gr', '32 X 13 X 42', 6),
(341, 379, 'CAI 024', 'MERAH', 'CANVAS', '500 gr', '32 X 13 X 42', 6),
(342, 380, 'CIN 032', 'BIRU', 'DINIR', '500 gr', '34 X 13 X 42', 6),
(343, 1, 'JK 535', 'ABU ABU', 'SINTETIS', '700 gr', 'SOL : RUBBER, 5 CM', 7),
(344, 2, 'MC 733', 'ABU ABU', 'SINTETIS', '0 gr', 'SOL : FIBER, 7 CM', 7),
(345, 3, 'LD 081', 'MERAH', 'SINTETIS', '700 gr', 'SOL : TPR, 5 CM', 7),
(346, 4, 'KK 1615', 'TAN', 'SINTETIS', '700 gr', 'SOL : TPR, 5 CM', 7),
(347, 5, 'AY 583', 'CREAM', 'SINTETIS', '0 gr', 'SOL : RUBBER, 8 CM', 7),
(348, 6, 'AY 564', 'BIRU', 'DENIM', '0 gr', 'SOL : FIBER, 8 CM', 7),
(349, 7, 'TG 175', 'COKLAT', 'SINTETIS', '700 gr', 'SOL : TPR, 7 CM', 7),
(350, 8, 'JK 521', 'PUTIH', 'SINTETIS', '0 gr', 'SOL : FIBER, 8 CM', 7),
(351, 9, 'JK 524', 'HITAM', 'SINTETIS', '0 gr', 'SOL : FIBER, 8 CM', 7),
(352, 10, 'NN 030', 'COKLAT', 'SINTETIS', '0 gr', 'SOL : RUBBER, 7 CM', 7),
(353, 11, 'JK 531', 'PUTIH', 'SINTETIS', '0 gr', 'SOL : FIBER, 8 CM', 7),
(354, 12, 'NN 040', 'BIRU', 'DENIM', '700 gr', 'SOL : RUBBER, 7 CM', 7),
(355, 13, 'KM 042', 'CREAM', 'SINTETIS', '720 gr', 'SOL : FIBER, 11 CM', 7),
(356, 14, 'NN 039', 'COKLAT', 'SINTETIS', '700 gr', 'SOL : RUBBER, 7 CM', 7),
(357, 15, 'AC 822', 'HITAM', 'SINTETIS', '0 gr', 'SOL : FIBER,  9 CM', 7),
(358, 16, 'AC 809', 'CREAM', 'SINTETIS', '0 gr', 'SOL : FIBER, 9 CM', 7),
(359, 17, 'KM 035', 'CREAM', 'SINTETIS', '0 gr', 'SOL : FIBER, 9 CM', 7),
(360, 18, 'AY 597', 'COKLAT', 'SINTETIS', '0 gr', 'SOL : FIBER, 10 CM', 7),
(361, 19, 'KM 055', 'TAN', 'SINTETIS', '800 gr', 'SOL : FIBER, 11 CM', 7),
(362, 20, 'KM 044', 'CREAM', 'SINTETIS', '1030 gr', 'SOL : FIBER, 11 CM', 7);

-- --------------------------------------------------------

--
-- Table structure for table `table_stok`
--

CREATE TABLE IF NOT EXISTS `table_stok` (
  `id` int(11) NOT NULL,
  `kode` varchar(30) NOT NULL,
  `size` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `pesanan` int(11) NOT NULL,
  `last_update` text NOT NULL,
  `scraping_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=590 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_stok`
--

INSERT INTO `table_stok` (`id`, `kode`, `size`, `stok`, `pesanan`, `last_update`, `scraping_id`) VALUES
(470, 'CIN 030', 0, 9, 0, '18 Jan 18, 12:13:37', 6),
(471, 'CAI 025', 0, 23, 0, '18 Jan 18, 16:45:00', 6),
(472, 'CCL 001', 0, 33, 0, '18 Jan 18, 13:10:39', 6),
(473, 'CCL 002', 0, 39, 0, '18 Jan 18, 13:10:37', 6),
(474, 'CYD 276', 0, 15, 0, '17 Jan 18, 18:03:46', 6),
(475, 'CST 007', 0, 2, 0, '18 Jan 18, 15:22:43', 6),
(476, 'CYD 275', 0, 7, 0, '15 Jan 18, 13:45:19', 6),
(477, 'CST 274', 0, 2, 0, '18 Jan 18, 13:10:39', 6),
(478, 'CRZ 189', 0, 5, 0, '18 Jan 18, 15:51:30', 6),
(479, 'CST 006', 0, 10, 0, '18 Jan 18, 14:06:34', 6),
(480, 'CTP 270', 0, 15, 0, '18 Jan 18, 09:22:40', 6),
(481, 'CYD 277', 0, 9, 0, '05 Jan 18, 17:26:17', 6),
(482, 'CTP 268', 0, 4, 0, '18 Jan 18, 16:16:52', 6),
(483, 'CTP 271', 0, 10, 0, '17 Jan 18, 16:21:58', 6),
(484, 'CBD 174', 0, 0, 0, '16 Jan 18, 15:53:32', 6),
(485, 'CBD 171', 0, 52, 0, '18 Jan 18, 11:48:37', 6),
(486, 'CCL 011', 0, 6, 0, '18 Jan 18, 09:47:06', 6),
(487, 'CAI 026', 0, 65, 0, '13 Jan 18, 14:47:13', 6),
(488, 'CAI 024', 0, 27, 2, '18 Jan 18, 13:22:47', 6),
(489, 'CIN 032', 0, 5, 1, '17 Jan 18, 12:49:49', 6),
(490, 'JK 535', 36, 5, 0, '18 Jan 18, 12:16:04', 7),
(491, 'JK 535', 37, 7, 0, '14 Jan 18, 11:42:18', 7),
(492, 'JK 535', 38, 7, 0, '15 Jan 18, 15:29:00', 7),
(493, 'JK 535', 39, 9, 0, '16 Jan 18, 10:17:01', 7),
(494, 'JK 535', 40, 5, 0, '17 Jan 18, 15:01:28', 7),
(495, 'MC 733', 36, 7, 0, '08 Jan 18, 09:12:40', 7),
(496, 'MC 733', 37, 13, 0, '14 Jan 18, 09:45:16', 7),
(497, 'MC 733', 38, 10, 0, '11 Jan 18, 09:18:27', 7),
(498, 'MC 733', 39, 15, 0, '07 Jan 18, 11:27:37', 7),
(499, 'MC 733', 40, 10, 0, '15 Jan 18, 16:56:17', 7),
(500, 'LD 081', 36, 7, 0, '30 Dec 17, 11:24:30', 7),
(501, 'LD 081', 37, 10, 0, '08 Jan 18, 16:14:22', 7),
(502, 'LD 081', 38, 6, 0, '17 Jan 18, 15:01:28', 7),
(503, 'LD 081', 39, 12, 0, '13 Jan 18, 11:04:18', 7),
(504, 'LD 081', 40, 9, 0, '13 Jan 18, 11:04:18', 7),
(505, 'KK 1615', 36, 6, 0, '06 Jan 18, 10:08:21', 7),
(506, 'KK 1615', 37, 6, 0, '02 Jan 18, 17:15:22', 7),
(507, 'KK 1615', 38, 5, 0, '06 Jan 18, 10:08:21', 7),
(508, 'KK 1615', 39, 8, 0, '16 Jan 18, 09:25:12', 7),
(509, 'KK 1615', 40, 4, 0, '06 Jan 18, 10:08:21', 7),
(510, 'AY 583', 36, 5, 0, '12 Jan 18, 16:31:46', 7),
(511, 'AY 583', 37, 4, 0, '16 Jan 18, 10:14:39', 7),
(512, 'AY 583', 38, 8, 0, '17 Jan 18, 16:39:11', 7),
(513, 'AY 583', 39, 10, 0, '13 Jan 18, 15:43:22', 7),
(514, 'AY 583', 40, 7, 0, '02 Jan 18, 15:30:00', 7),
(515, 'AY 564', 36, 3, 0, '31 Dec 17, 10:50:28', 7),
(516, 'AY 564', 37, 0, 0, '10 Jan 18, 08:48:43', 7),
(517, 'AY 564', 38, 6, 0, '18 Jan 18, 11:28:27', 7),
(518, 'AY 564', 39, 1, 0, '18 Jan 18, 09:29:21', 7),
(519, 'AY 564', 40, 8, 1, '17 Jan 18, 09:53:20', 7),
(520, 'TG 175', 36, 3, 0, '15 Jan 18, 12:09:54', 7),
(521, 'TG 175', 37, 1, 0, '15 Jan 18, 12:09:54', 7),
(522, 'TG 175', 38, 5, 0, '05 Jan 18, 15:36:07', 7),
(523, 'TG 175', 39, 2, 0, '12 Jan 18, 18:16:31', 7),
(524, 'TG 175', 40, 2, 0, '12 Jan 18, 10:41:16', 7),
(525, 'JK 521', 36, 5, 0, '05 Jan 18, 10:00:53', 7),
(526, 'JK 521', 37, 1, 0, '13 Jan 18, 15:25:02', 7),
(527, 'JK 521', 38, 4, 1, '11 Jan 18, 15:17:20', 7),
(528, 'JK 521', 39, 3, 0, '18 Jan 18, 11:13:13', 7),
(529, 'JK 521', 40, 1, 1, '12 Jan 18, 15:12:19', 7),
(530, 'JK 524', 36, 5, 0, '11 Jan 18, 14:38:11', 7),
(531, 'JK 524', 37, 7, 0, '12 Jan 18, 12:43:15', 7),
(532, 'JK 524', 38, 4, 0, '12 Jan 18, 13:52:57', 7),
(533, 'JK 524', 39, 10, 0, '13 Jan 18, 13:57:29', 7),
(534, 'JK 524', 40, 6, 0, '16 Jan 18, 14:03:14', 7),
(535, 'NN 030', 36, 5, 1, '11 Jan 18, 15:17:20', 7),
(536, 'NN 030', 37, 7, 1, '13 Jan 18, 09:55:14', 7),
(537, 'NN 030', 38, 8, 0, '08 Jan 18, 11:50:58', 7),
(538, 'NN 030', 39, 7, 0, '09 Jan 18, 15:04:40', 7),
(539, 'NN 030', 40, 5, 0, '18 Jan 18, 10:24:46', 7),
(540, 'JK 531', 36, 5, 0, '15 Jan 18, 12:24:44', 7),
(541, 'JK 531', 37, 8, 0, '13 Jan 18, 18:04:03', 7),
(542, 'JK 531', 38, 7, 0, '12 Jan 18, 13:11:23', 7),
(543, 'JK 531', 39, 8, 0, '17 Jan 18, 16:47:06', 7),
(544, 'JK 531', 40, 6, 0, '15 Jan 18, 11:18:44', 7),
(545, 'NN 040', 36, 5, 0, '09 Jan 18, 14:58:22', 7),
(546, 'NN 040', 37, 4, 0, '15 Jan 18, 09:12:01', 7),
(547, 'NN 040', 38, 5, 0, '13 Jan 18, 13:57:29', 7),
(548, 'NN 040', 39, 8, 0, '11 Dec 17, 12:13:41', 7),
(549, 'NN 040', 40, 4, 0, '03 Jan 18, 11:06:12', 7),
(550, 'KM 042', 36, 8, 0, '18 Jan 18, 10:50:51', 7),
(551, 'KM 042', 37, 13, 0, '12 Jan 18, 15:12:47', 7),
(552, 'KM 042', 38, 13, 1, '18 Jan 18, 10:50:51', 7),
(553, 'KM 042', 39, 10, 0, '18 Jan 18, 10:50:52', 7),
(554, 'KM 042', 40, 11, 0, '18 Jan 18, 10:50:52', 7),
(555, 'NN 039', 36, 5, 0, '22 Dec 17, 16:50:19', 7),
(556, 'NN 039', 37, 7, 0, '15 Jan 18, 18:12:38', 7),
(557, 'NN 039', 38, 6, 1, '18 Jan 18, 11:36:26', 7),
(558, 'NN 039', 39, 4, 0, '18 Jan 18, 15:12:25', 7),
(559, 'NN 039', 40, 0, 0, '18 Jan 18, 11:28:27', 7),
(560, 'AC 822', 36, 3, 0, '11 Jan 18, 14:14:13', 7),
(561, 'AC 822', 37, 7, 0, '18 Jan 18, 09:18:20', 7),
(562, 'AC 822', 38, 10, 0, '18 Jan 18, 08:35:14', 7),
(563, 'AC 822', 39, 8, 0, '16 Jan 18, 14:07:18', 7),
(564, 'AC 822', 40, 3, 0, '17 Jan 18, 09:24:24', 7),
(565, 'AC 809', 36, 4, 0, '18 Jan 18, 10:50:34', 7),
(566, 'AC 809', 37, 3, 0, '12 Jan 18, 16:11:46', 7),
(567, 'AC 809', 38, 4, 0, '18 Jan 18, 14:14:36', 7),
(568, 'AC 809', 39, 9, 1, '03 Jan 18, 14:58:16', 7),
(569, 'AC 809', 40, 7, 0, '17 Jan 18, 12:37:38', 7),
(570, 'KM 035', 36, 9, 0, '03 Jan 18, 09:19:27', 7),
(571, 'KM 035', 37, 9, 0, '16 Jan 18, 15:39:22', 7),
(572, 'KM 035', 38, 9, 0, '17 Jan 18, 13:52:39', 7),
(573, 'KM 035', 39, 4, 0, '14 Jan 18, 11:26:16', 7),
(574, 'KM 035', 40, 7, 0, '18 Jan 18, 16:21:00', 7),
(575, 'AY 597', 36, 6, 0, '06 Jan 18, 14:33:40', 7),
(576, 'AY 597', 37, 0, 0, '17 Jan 18, 10:03:29', 7),
(577, 'AY 597', 38, 0, 0, '11 Jan 18, 10:38:34', 7),
(578, 'AY 597', 39, 0, 0, '30 Dec 17, 11:12:58', 7),
(579, 'AY 597', 40, 1, 0, '11 Jan 18, 10:39:05', 7),
(580, 'KM 055', 36, 4, 0, '29 Dec 17, 13:36:03', 7),
(581, 'KM 055', 37, 6, 0, '07 Jan 18, 08:53:45', 7),
(582, 'KM 055', 38, 7, 0, '22 Nov 17, 17:49:50', 7),
(583, 'KM 055', 39, 5, 0, '27 Dec 17, 09:47:08', 7),
(584, 'KM 055', 40, 4, 0, '21 Dec 17, 15:44:40', 7),
(585, 'KM 044', 36, 6, 1, '11 Jan 18, 15:17:20', 7),
(586, 'KM 044', 37, 7, 0, '14 Jan 18, 11:20:22', 7),
(587, 'KM 044', 38, 11, 0, '18 Jan 18, 10:50:52', 7),
(588, 'KM 044', 39, 9, 0, '18 Jan 18, 10:50:52', 7),
(589, 'KM 044', 40, 9, 0, '18 Jan 18, 10:50:52', 7);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `pegawai_id`, `username`, `password`, `role`) VALUES
(5, 2, 'admin', '$2y$10$OXrkAgDJIBk7N/5A9eje8e3wmih1Q/YhHGKx2xtbroQj0CKfs9fgm', 'adm'),
(6, 20, 'hhhh', '421', 'adm'),
(7, 21, 'cebong', '123', 'adm'),
(8, 22, 'anto', 'anto', 'adm');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scraping`
--
ALTER TABLE `scraping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`);

--
-- Indexes for table `table_keterangan`
--
ALTER TABLE `table_keterangan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode` (`kode`),
  ADD KEY `scraping_id` (`scraping_id`);

--
-- Indexes for table `table_stok`
--
ALTER TABLE `table_stok`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode` (`kode`),
  ADD KEY `scraping_id` (`scraping_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pegawai_id` (`pegawai_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `scraping`
--
ALTER TABLE `scraping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `table_keterangan`
--
ALTER TABLE `table_keterangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=363;
--
-- AUTO_INCREMENT for table `table_stok`
--
ALTER TABLE `table_stok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=590;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `scraping`
--
ALTER TABLE `scraping`
  ADD CONSTRAINT `fk_users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `table_keterangan`
--
ALTER TABLE `table_keterangan`
  ADD CONSTRAINT `scraping_id_fk` FOREIGN KEY (`scraping_id`) REFERENCES `scraping` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `table_stok`
--
ALTER TABLE `table_stok`
  ADD CONSTRAINT `kode_brg_fk` FOREIGN KEY (`kode`) REFERENCES `table_keterangan` (`kode`) ON UPDATE CASCADE,
  ADD CONSTRAINT `scraping_id_fk1` FOREIGN KEY (`scraping_id`) REFERENCES `scraping` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_pegawai_id` FOREIGN KEY (`pegawai_id`) REFERENCES `pegawai` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
