Web Scraping
------------

==Code Igniter Website Sraping==


Installation
-------------
- Jalankan Xampp, module apache dan mysql
- Copy folder 'anto' ke folder 'httdocs'

Pengaturan Database
---------------------
- Buka http://localhost/phpmyadmin di browser
- Buat database baru dengan nama 'web_scraping'
- Masuk ke menu 'import'
- Pilih file 'database.sql' yang ada di folder 'anto'

Menggunakan Aplikasi
---------------------
- Buka http://localhost/anto di browser
- Masukan halaman yang akan di scraping di field url, contoh: http://cjr.catenzo.co.id/stok.php
- Jumlah hal adalah maximal halaman yang akan di scrap