<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scraping_model extends CI_Model {

	public $table = 'table_keterangan';
	public $id = 'id';
	public $order = 'ASC';

	function __construct()
	{
		parent::__construct();
	}

	function get_ket()
	{
		$this->db->order_by($this->id, $this->order);
		return $this->db->get($this->table)->result();
	}

	function get_stok()
	{
		$this->db->order_by($this->id, $this->order);
		return $this->db->get('table_stok')->result();
	}

	function get_scrap()
	{
		$this->db->order_by('scraping.id', $this->order);
		$this->db->join('users','users.id=scraping.users_id');
		$this->db->join('pegawai','pegawai.id=users.pegawai_id');
		return $this->db->get('scraping')->result();
	}

	function insert($data)
	{
		// $this->db->insert($this->table, $data);
		$this->db->insert_batch($this->table, $data);
	}

	function insert_stok($id, $arr_data)
	{
		foreach ($id as  $key => $val) {
			$arr_kode[] = $id[$key++];
		}
		foreach ($arr_data as $no => $value) {
			$kode = $arr_kode[$no++];
			$data = array();
			foreach ($value as $r) {
				$data[] = array(
					'kode' => $kode,
					'size' => $r['size'],
					'stok' => $r['stok'],
					'pesanan' => $r['pesanan'],
					'last_update' => $r['last_update'],
					'scraping_id' => $r['scraping_id'],
				);
				// $this->db->insert('table_stok', $data);
			}
			$this->db->insert_batch('table_stok', $data);
		}
	}

	function insert_scraping($data)
	{
		$this->db->insert('scraping',$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
		// return $this->db->insert_id();
	}

	function get_user_id()
	{
		$this->db->select('id');
		$this->db->limit(1);
		$result = $this->db->get('scraping')->result();
		return $result;
	}

}

/* End of file scraping_model.php */
/* Location: ./application/models/scraping_model.php */