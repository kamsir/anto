<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model {

	private $table = 'users';
	private $user_info = array();

	function insert($data)
	{
		$this->db->insert($this->table, $data);
	}

	function validate($username, $password)
	{
		$this->db->where('username',$username);
		$this->db->limit(1);
		$result = $this->db->get($this->table);
		if ($result->num_rows() == 1) {
			$row = $result->row_array();
			if (password_verify($password, $row['password'])) {
				$this->session->set_flashdata('message', callout('info','Hello '.$row['username']));
				unset($row['password']);
				$this->user_info = $row;
				return $this->user_info;
			}
			else {
				$this->session->set_flashdata('error', callout('danger','Username or Password are Incorrect'));
				return NULL;
			}
		}
		else{
			$this->session->set_flashdata('error', callout('danger','Username or Password are Incorrect'));
		}
	}

	function get_data()
	{
		return $this->user_info;
	}

}
/* End of file Auth_mode.php */
/* Location: ./application/models/Auth_mode.php */