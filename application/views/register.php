<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/ionicons/css/ionicons.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/select2/select2.min.css') ?>"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css') ?>"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/datatables/extensions/Responsive/css/dataTables.responsive.css') ?>"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/adminlte/css/AdminLTE.min.css') ?>"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/adminlte/css/skins/_all-skins.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/datepicker/datepicker3.css') ?>"/>

	<script src="<?php echo base_url('assets/js/jquery-2.2.3.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/plugins/select2/select2.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
	<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
	<script src="<?php echo base_url('assets/adminlte/js/app.min.js')?>"></script>
	<script src="<?php echo base_url('assets/plugins/bootstrap-toggle/bootstrap-toggle.min.js')?>"></script>
	<script src="<?php echo base_url('assets/datatables/extensions/Responsive/js/dataTables.responsive.min.js') ?>"></script>
</head>
<body>
	<form action="<?= $action?>" method="POST" role="form">
		<legend>Register</legend>

		<div class="form-group">
			<label for="username">Username <?php echo form_error('username');?></label>
			<input type="text" class="form-control" id="username" placeholder="Username" name="username">
		</div>
		<div class="form-group">
			<label for="username">Password <?php echo form_error('password');?></label>
			<input type="password" class="form-control" id="password" placeholder="Password" name="password">
		</div>
		<button type="submit" class="btn btn-primary">Submit</button>
	</form>
</body>
</html>