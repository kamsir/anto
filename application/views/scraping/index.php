<section class="content-header">
	<h1>
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><i class="fa fa-dashboard"></i> Scraping</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<section class="col-md-12">
			<div class="box box-primary box-solid">
				<div class="box-header with-border text-center">
					<h3 class="box-title">Form Scraping</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="row" style="margin-bottom: 10px">
						<div class="col-md-offset-3 col-md-6 text-center">
							<?= $this->session->userdata('message') ? $this->session->userdata('message') : ''; ?>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12" style="margin-bottom: 10px">
							<form action="<?= $action?>" method="POST" role="form" class="form-horizontal">
								<div class="form-group">
									<label for="url" class="control-label col-sm-2">URL</label>
									<div class="col-sm-4">
										<input type="text" name="url" class="form-control" id="url" placeholder="Masukan URL" required>
									</div>
								</div>
								<div class="form-group">
									<label for="page" class="control-label col-sm-2">Jumlah Hal</label>
									<div class="col-sm-4">
										<input type="number" name="page" class="form-control" id="page" placeholder="Jumlah Halaman" required>
									</div>
								</div>
								<div class="form-group">
									<label for="note" class="control-label col-sm-2">Note</label>
									<div class="col-sm-4">
										<textarea name="note" class="form-control" placeholder="Note"></textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-2 col-sm-offset-2">
										<button type="submit" class="btn btn-primary fo">Submit</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="box box-primary">
								<div class="box-header with-border text-center">
									<h3 class="box-title">Table Scraping</h3>
								</div>
								<div class="box-body">
									<table class="table table-bordered" id="table_scrap">
										<thead>
											<th>No</th>
											<th>User</th>
											<th>Product</th>
											<th>Create Date</th>
										</thead>
										<tbody>
											<?php foreach ($data_scrap as $key => $value): ?>
												<tr>
													<td><?= $key+++1 ?></td>
													<td><?= $value->nama ?></td>
													<td><?= $value->product ?></td>
													<td><?= $value->create_date ?></td>
												</tr>
											<?php endforeach ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header with-border text-center">
									<h3 class="box-title">Table Keterangan</h3>
								</div>
								<div class="box-body">
									<table class="table table-bordered" id="table_keterangan">
										<thead>
											<th>No</th>
											<th>Kode</th>
											<th>Warna</th>
											<th>berat</th>
											<th>info</th>
										</thead>
										<tbody>
											<?php foreach ($data_ket as $key => $value): ?>
												<tr>
													<td><?= $value->no ?></td>
													<td><?= $value->kode ?></td>
													<td><?= $value->warna ?></td>
													<td><?= $value->berat ?></td>
													<td><?= $value->info ?></td>
												</tr>
											<?php endforeach ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header with-border text-center">
									<h3 class="box-title">Table Stok</h3>
								</div>
								<div class="box-body">
									<table class="table table-bordered" id="table_stok">
										<thead>
											<th>No</th>
											<th>Kode</th>
											<th>Size</th>
											<th>Stok</th>
											<th>Pesanan</th>
											<th>last_update</th>
										</thead>
										<tbody>
											<?php foreach ($data_stok as $key => $value): ?>
												<tr>
													<td><?= $key+++1 ?></td>
													<td><?= $value->kode ?></td>
													<td><?= $value->size ?></td>
													<td><?= $value->stok ?></td>
													<td><?= $value->pesanan ?></td>
													<td><?= $value->last_update ?></td>
												</tr>
											<?php endforeach ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</section>