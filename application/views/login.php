<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/ionicons/css/ionicons.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/adminlte/css/AdminLTE.min.css') ?>"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/adminlte/css/skins/_all-skins.min.css');?>">
	<script src="<?php echo base_url('assets/js/jquery-2.2.3.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/adminlte/js/app.min.js')?>"></script>
</head>
<body class="hold-transition login-page layout-top-nav skin-blue">
	<div class="login-box ">
		<div class="login-logo">
			<b>WEB</b> Scraping
		</div>
		<div class="login-box-body box box-primary box-solid">
			<p class="login-box-msg">Sign in to start your session
				<?php echo $this->session->flashdata('error'); ?></p>
			<form action="<?= $action?>" method="post" accept-charset="utf-8" role="form">
				<div class="form-group has-feedback">
					<input type="text" class="form-control" name="username" placeholder="Username" required="required">
					<span class="glyphicon glyphicon-user form-control-feedback"></span> <?php echo form_error('username');?></label>
				</div>
				<div class="form-group has-feedback">
					<input type="password" class="form-control" name="password" placeholder="Password" required="required">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span> <?php echo form_error('password');?>
				</div>
				<div class="row">
					<div class="col-xs-8">
					</div>
					<div class="col-xs-4">
						<button name="submit" type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>