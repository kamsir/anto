<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title"></h3>
			</div>
			<form id="form" class="form-horizontal">
				<div class="modal-body form">
					<div class="form-body">
						<input type="hidden" name="id" value="<?php echo $id; ?>" />
						<div class="form-group">
							<label for="pegawai_id" class="control-label col-md-3">Pegawai Id <?php echo form_error('pegawai_id') ?></label>
							<div class="col-md-9">
								<?php echo form_dropdown('pegawai_id', $options, $option_selected,'class="select2 form-control" required');?>
								<span class="text-danger" id="pegawai_id_msg"></span>
							</div>
						</div>
						<div class="form-group">
							<label for="username" class="control-label col-md-3">Username <?php echo form_error('username') ?></label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" required/>
								<span class="text-danger" id="username_msg"></span>
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="control-label col-md-3">Password <?php echo form_error('password') ?></label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" required/>
								<span class="text-danger" id="password_msg"></span>
							</div>
						</div>
						<div class="form-group">
							<label for="role" class="control-label col-md-3">Role <?php echo form_error('role') ?></label>
							<div class="col-md-9">
								<?= form_dropdown('role', options(), '' ,'class="select2 form-control" required');?>
								<span class="text-danger" id="role_msg"></span>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary"><?= $button?></button>
						<!-- <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button> -->
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('.select2').select2({
		placeholder:"Please Select"
	});
	$(document).ready(function() {
		$('form').submit(function(e){
			e.preventDefault();
			var url;
			if(save_method == 'add'){
				url = "<?php echo site_url('users/create_action')?>";
			}
			else{
				url = "<?php echo site_url('users/update_action')?>";
			}

			$.ajax({
				url : url,
				type: "POST",
				data: $('#form').serialize(),
				dataType: "JSON",
				success: function(data)
				{
					console.log(data);
					if (data['status'] == true) {
						$('#modal_form').modal('hide');
						$('#modal-backdrop').remove();
						$('.message').html(data['message']);
						table.ajax.reload();
					}
					else
					{
						// alert(1);
						$('#pegawai_id_msg').text(data['pegawai_id'])
						$('#username_msg').text(data['username'])
						$('#password_msg').text(data['password'])
						$('#role_msg').text(data['role'])
					}
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Error adding / update data');
				}
			});
		});
	});
</script>