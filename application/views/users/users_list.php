<section class="content-header">
	<h1>
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><i class="fa fa-user-o"></i> Pengguna</li>
		<li class="active">Pengguna</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<section class="col-md-12">
			<div class="box box-primary ">
				<div class="box-header with-border">
					<h3 class="box-title">Data Pengguna</h3>
				</div>
				<div class="box-body" id="result">
					<div class="modal-container"></div>
					<div class="row" style="margin-bottom: 10px">
						<div class="message col-md-offset-3 col-md-6 text-center">
							<?= $this->session->flashdata('message') ? $this->session->flashdata('message') : '';
							$this->session->unset_userdata('message');
							?>
						</div>
						<div class="col-md-3 text-right">
							<a href="#" class="btn btn-success" onclick="create()">Create</a>
						</div>
					</div>
					<table class="table table-bordered table-striped" id="mytable">
						<thead>
							<tr>
								<th width="80px">No</th>
								<th>NIK Pegawai</th>
								<th>Username</th>
								<th>Role</th>
								<th width="200px">Action</th>
							</tr>
						</thead>

					</table>
				</div>
			</div>
		</section>
	</div>
</section>
<script type="text/javascript">
	var table;
	$(document).ready(function() {
		$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
		{
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
		};

		table = $("#mytable").DataTable({
			initComplete: function() {
				var api = this.api();
				$('#mytable_filter input')
				.off('.DT')
				.on('keyup.DT', function(e) {
					if (e.keyCode == 13) {
						api.search(this.value).draw();
					}
				});
			},
			oLanguage: {
				sProcessing: "loading..."
			},
			processing: true,
			serverSide: true,
			ajax: {"url": "users/json", "type": "POST"},
			columns: [
			{
				"data": "id",
				"orderable": false
			},{"data": "nik"},{"data": "username"},{"data": "role"},
			{
				"data" : "action",
				"orderable": false,
				"className" : "text-center"
			}
			],
			order: [[0, 'desc']],
			rowCallback: function(row, data, iDisplayIndex) {
				var info = this.fnPagingInfo();
				var page = info.iPage;
				var length = info.iLength;
				var index = page * length + (iDisplayIndex + 1);
				$('td:eq(0)', row).html(index);
			}
		});
	});

	var save_method;

	function create(){
		save_method = 'add';
		var url = "<?= base_url('users/create'); ?>";
		$('.modal-container').load(url,function(result){
			$('#modal_form').modal({show:true});
			$('.modal-title').text('Add Pengguna');
			$('#modal_form').on('hidden.bs.modal', function (e) {
				$('#modal_form').remove();
			})
		});
	}
</script>