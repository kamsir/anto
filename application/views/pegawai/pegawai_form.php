<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title"></h3>
			</div>
			<form id="form" class="form-horizontal">
				<div class="modal-body form">
					<input type="hidden" value="" name="id"/>
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3">NIK</label>
							<div class="col-md-9">
								<input name="nik" placeholder="NIK" class="form-control" type="number" required>
								<span class="text-danger" id="nik_msg"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Nama</label>
							<div class="col-md-9">
								<input name="nama" placeholder="Nama" class="form-control" type="text" required>
								<span class="text-danger" id="nama_msg"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary"><?= $button?></button>
					<!-- <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button> -->
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('form').submit(function(e){
			e.preventDefault();
			var url;
			if(save_method == 'add'){
				url = "<?php echo site_url('pegawai/create_action')?>";
			}
			else{
				url = "<?php echo site_url('pegawai/update_action')?>";
			}

			$.ajax({
				url : url,
				type: "POST",
				data: $('#form').serialize(),
				dataType: "JSON",
				success: function(data)
				{
					if (data['status'] == true) {
						$('#modal_form').modal('hide');
						$('#modal-backdrop').remove();
						$('.message').html(data['message']);
						table.ajax.reload();
						// location.reload();// for reload a page
					}
					else
					{
						$('#nik_msg').text(data['nik'])
						$('#nama_msg').text(data['nama'])
					}
					// $('#modal_form').modal('hide');
					// location.reload();// for reload a page
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Error adding / update data');
				}
			});
		});
	});
</script>