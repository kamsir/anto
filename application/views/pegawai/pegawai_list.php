<section class="content-header">
	<h1>
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><i class="fa fa-user-o"></i> Pengguna</li>
		<li class="active">Pegawai</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<section class="col-md-12">
			<div class="box box-primary ">
				<div class="box-header with-border">
					<h3 class="box-title">Data Pegawai</h3>
				</div>
				<div class="box-body" id="result">
					<div class="modal-container"></div>
					<div class="row" style="margin-bottom: 10px">
						<div class="message col-md-offset-3 col-md-6 text-center">
							<?= $this->session->flashdata('message') ? $this->session->flashdata('message') : '';
							$this->session->unset_userdata('message');
							?>
						</div>
						<div class="col-md-3 text-right">
							<a href="#" class="btn btn-success" onclick="create()">Create</a>
						</div>
					</div>
					<table class="table table-bordered table-striped" id="mytable">
						<thead>
							<tr>
								<th width="80px">No</th>
								<th>Nik</th>
								<th>Nama</th>
								<th width="200px">Action</th>
							</tr>
						</thead>

					</table>
				</div>
			</div>

		</section>
	</div>
</section>
<script src="<?php echo base_url('assets/plugins/form/jquery.form.min.js') ?>"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function() {
		$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
		{
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
		};

		table = $("#mytable").DataTable({
			initComplete: function() {
				var api = this.api();
				$('#mytable_filter input')
				.off('.DT')
				.on('keyup.DT', function(e) {
					if (e.keyCode == 13) {
						api.search(this.value).draw();
					}
				});
			},
			oLanguage: {
				sProcessing: "loading..."
			},
			processing: true,
			serverSide: true,
			ajax: {"url": "pegawai/json", "type": "POST"},
			columns: [
			{
				"data": "id",
				"orderable": false
			},{"data": "nik"},{"data": "nama"},
			{
				"data" : "action",
				"orderable": false,
				"className" : "text-center"
			}
			],
			order: [[0, 'desc']],
			rowCallback: function(row, data, iDisplayIndex) {
				var info = this.fnPagingInfo();
				var page = info.iPage;
				var length = info.iLength;
				var index = page * length + (iDisplayIndex + 1);
				$('td:eq(0)', row).html(index);
			}
		});
	});


	function create(){
		save_method = 'add';
		var url = "<?= base_url('pegawai/create'); ?>";
		$('.modal-container').load(url,function(result){
			$('#modal_form').modal({show:true});
			$('.modal-title').text('Add Pegawai');
			$('#modal_form').on('hidden.bs.modal', function (e) {
				$('#modal_form').remove();
			})
		});
	}

	// function create()
	// {
	// 	save_method = 'add';
	// 	$('#form')[0].reset(); // reset form on modals
	// 	$('#modal_form').modal('show'); // show bootstrap modal
	// 	//$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
	// }

	var save_method;

	function save()
	{
		var url;
		if(save_method == 'add'){
			url = "<?php echo site_url('pegawai/create_action')?>";
		}
		else{
			url = "<?php echo site_url('pegawai/update_action')?>";
		}
		// ajax adding data to database
		$.ajax({
			url : url,
			type: "POST",
			data: $('#form').serialize(),
			dataType: "JSON",
			success: function(data)
			{
				// var obj = $.parseJSON(data);
				//if success close modal and reload ajax table
				// if (true) {}
				$('#modal_form').modal('hide');
				// location.reload();// for reload a page
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error adding / update data');
			}
		});
	}
</script>