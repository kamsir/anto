<!DOCTYPE html>
<html>
<head>
	<title>Web Scraping Anto</title>

	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/ionicons/css/ionicons.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/select2/select2.min.css') ?>"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css') ?>"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/datatables/extensions/Responsive/css/dataTables.responsive.css') ?>"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/adminlte/css/AdminLTE.min.css') ?>"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/adminlte/css/skins/_all-skins.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/datepicker/datepicker3.css') ?>"/>

	<script src="<?php echo base_url('assets/js/jquery-2.2.3.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/plugins/select2/select2.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
	<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
	<script src="<?php echo base_url('assets/adminlte/js/app.min.js')?>"></script>
	<script src="<?php echo base_url('assets/plugins/bootstrap-toggle/bootstrap-toggle.min.js')?>"></script>
	<script src="<?php echo base_url('assets/datatables/extensions/Responsive/js/dataTables.responsive.min.js') ?>"></script>

	<style>
	.table{
		width: 100% !important;
	}
	.dataTables_wrapper {
		min-height: 500px
	}

	.dataTables_processing {
		position: absolute;
		top: 50%;
		left: 50%;
		width: 100%;
		margin-left: -50%;
		margin-top: -25px;
		padding-top: 20px;
		text-align: center;
		font-size: 1.2em;
		color:grey;
	}
	.select2-container{ width: 100% !important; }
</style>
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<a href="#" class="logo">
				<span class="logo-mini"><b>ADM</b></span>
				<span class="logo-lg"><b>AD</b>MIN</span>
			</a>
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<!-- Navbar Right Menu -->
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li class="dropdown user user-menu pull-right">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="glyphicon glyphicon-log-out"></i>
								<span class="hidden-xs">Sign Out</span>
							</a>
							<ul class="dropdown-menu">
								<li class="user-body">
									<div class="row">
										<div class="col-xs-12 text-center">
											<a href="#">Anda Yakin Akan Keluar</a>
										</div>
									</div>
								</li>
								<li class="user-footer">
									<div class="pull-right">
										<a href="<?= site_url('auth/logout')?>" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>

		<aside class="main-sidebar">
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li class="header text-center">MAIN NAVIGATION</li>
					<li class="<?php echo activate_menu('dashboard'); ?>"><a href="<?php echo site_url('main') ?>"><i class="fa fa-dashboard"></i><span> Scraping</span></a></li>

					<li class="treeview <?= activate_menu('pegawai'),activate_menu('users');?>">
						<a href="#">
							<i class="fa fa-user-o"></i> <span>Pengguna</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li class="<?= activate_menu('pegawai');?>"><a href="<?= site_url('pegawai')?>"><i class="fa fa-square-o"></i>Pegawai</a></li>
							<li class="<?= activate_menu('users');?>"><a href="<?= site_url('users')?>"><i class="fa fa-square-o"></i>Pengguna</a></li>
						</ul>
					</li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<div class="content-wrapper">
			<?php
			$page_name .= ".php";
			include $page_name;
			?>
		</div>
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017.</strong> All rights
			reserved.
		</footer>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".select2").select2({
				placeholder: "Please Select"
			});
		});
		$('#table_scrap').DataTable();
		$('#table_keterangan').DataTable();
		$('#table_stok').DataTable();
	</script>
</body>
</html>