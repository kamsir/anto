<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

// public function __construct() {
//     parent::__construct();
// }

if(!function_exists('active_link')) {
	function activate_menu($controller) {
    // Getting CI class instance.
		$CI = get_instance();
    // Getting router class to active.
		$class = $CI->router->fetch_class();
		return ($class == $controller) ? 'active' : '';
	}
}

if(!function_exists('activate')) {
	function activate($controller) {
		return (uri_string() == $controller) ? 'active' : '';
	}
}

function status($id){
	if ($id == '1') {
		// $status = 'Aktif';
		$status = '<label class="label label-success">Aktif</label>';
	}
	elseif($id == '0'){
		$status = '<label class="label label-danger">Tidak Aktif</label>';
	}
	elseif ($id == 'DU') {
		$status = 'Daftar Ulang';
	}
	elseif ($id == 'PPDB') {
		$status = 'PPDB';
	}
	elseif ($id == 'PD') {
		$status = 'Pendaftaran';
	}
	else{
		$status = 'Error';
	}
	return $status;
}

function rupiah($nilai, $pecahan = 0){
	$hasil = "Rp. ".number_format($nilai, $pecahan, ',', '.');
   // return number_format($nilai, $pecahan, ',', '.');
	return $hasil;
}

function data_ppdb($id,$tipe){
	$CI = get_instance();
	$CI->load->model('Laporan_model');
	$list_data =  $CI->Laporan_model->get_data($id);
	if ($list_data){
		foreach ($list_data as $key => $value) {
			if ($tipe == 'kode') {
				$data = $value->nama_komponen;
			}
			elseif ($tipe == 'status') {
				$data = $value->status_bayar;
				if ($value->status_bayar == 'Lunas') {
					$data = "<label class='label label-success'>".$value->status_bayar."</label>";
				}
				elseif ($value->status_bayar == 'Nunggak') {
					$data = "<label class='label label-warning'>".$value->status_bayar."</label>";
				}
			}
			elseif ($tipe == 'biaya') {
				$hasil = ($value->nominal)-($value->jum_bayar);
				$data = rupiah($hasil);
				// $data = "<div class='col-md-3'>".$value->nominal."</div><div class='col-md-1'>-</div><div class='col-md-3'>".$value->jum_bayar."</div><div class='col-md-1'>=</div><div class='col-md-3'>".rupiah($hasil)."</div>";
			}
			else $data ='';
			// print_r($value->kode_komponen);
			echo $data."<br>";
			// echo ;
			// printf($data);
		}
	}
	else echo "<label class='label label-danger'>Belum Bayar</label>";
}

function eksel($id,$tipe){
	$CI = get_instance();
	$CI->load->model('Laporan_model');
	$list_data =  $CI->Laporan_model->get_data($id);
	if ($list_data){
		foreach ($list_data as $key => $value) {
			if ($tipe == 'kode') {
				$data = $value->nama_komponen;
			}
			elseif ($tipe == 'status') {
				$data = $value->status_bayar;
				if ($value->status_bayar == 'Lunas') {
					$data = $value->status_bayar;
				}
				elseif ($value->status_bayar == 'Nunggak') {
					$data = $value->status_bayar;
				}
			}
			elseif ($tipe == 'biaya') {
				$hasil = ($value->nominal)-($value->jum_bayar);
				$data = rupiah($hasil);
			}
			else $data ='';
			echo $data."<br>";
		}
	}
	else echo "Belum Bayar";

}

if(!function_exists('callout')) {
	function callout($key, $message) {
		return "<div class='callout callout-$key alert-dismissible text-center'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>$message</div>";
	}
}
function options(){
	$options = array(
		'' => "Please Select",
		'adm' => "Admin",
		'opr' => "Operator",
	);
	return $options;
}
function role($val){
	if ($val == 'adm') {
		$role = 'Admin';
	}
	elseif ($val == 'opr') {
		$role = 'Operator';
	}
	else{
		$role = "Error";
	}
	return $role;
}