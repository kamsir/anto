<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller
{
	protected $access = array('adm');

	function __construct()
	{
		parent::__construct();
		$this->load->model('Users_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index()
	{
		$data = array(
			'page_name' => 'users/users_list',
			'action'=> site_url('pegawai/process'),
		);
		$this->load->view('index',$data);
	}

	public function json() {
		header('Content-Type: application/json');
		echo $this->Users_model->json();
	}

	public function read($id)
	{
		$row = $this->Users_model->get_by_id($id);
		if ($row) {
			$data = array(
				'id' => $row->id,
				'pegawai_id' => $row->pegawai_id,
				'username' => $row->username,
				'password' => $row->password,
				'role' => $row->role,
			);
			$this->load->view('users/users_read', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('users'));
		}
	}

	public function create()
	{
		$data = array(
			'button' => 'Create',
			'action' => site_url('users/create_action'),
			'id' => set_value('id'),
			'pegawai_id' => set_value('pegawai_id'),
			'username' => set_value('username'),
			'password' => set_value('password'),
			'role' => set_value('role'),
			'options' => $this->Users_model->get_pegawai(),
			'option_selected' => $this->input->post('pegawai_id') ? $this->input->post('pegawai_id') : '',
		);
		$this->load->view('users/users_form', $data);
	}


	public function create_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$data = array(
				'pegawai_id' => form_error('pegawai_id'),
				'username' => form_error('username'),
				'password' => form_error('password'),
				'role' => form_error('role'),
			);
			echo json_encode($data);
		} else {
			$data = array(
				'pegawai_id' => $this->input->post('pegawai_id',TRUE),
				'username' => $this->input->post('username',TRUE),
				'password' => $this->input->post('password',TRUE),
				'role' => $this->input->post('role',TRUE),
			);
			$insert = $this->Users_model->insert($data);

			$json = array(
				"status" => TRUE,
				'message' => callout('info','Create Record Success'),
			);
			echo json_encode($json);
		}
	}

	// public function create_action()
	// {
	// 	$this->_rules();

	// 	if ($this->form_validation->run() == FALSE) {
	// 		$this->create();
	// 	} else {
	// 		$data = array(
	// 			'pegawai_id' => $this->input->post('pegawai_id',TRUE),
	// 			'username' => $this->input->post('username',TRUE),
	// 			'password' => $this->input->post('password',TRUE),
	// 			'role' => $this->input->post('role',TRUE),
	// 		);

	// 		$this->Users_model->insert($data);
	// 		$this->session->set_flashdata('message', 'Create Record Success');
	// 		redirect(site_url('users'));
	// 	}
	// }

	public function update($id)
	{
		$row = $this->Users_model->get_by_id($id);

		if ($row) {
			$data = array(
				'button' => 'Update',
				'action' => site_url('users/update_action'),
				'id' => set_value('id', $row->id),
				'pegawai_id' => set_value('pegawai_id', $row->pegawai_id),
				'username' => set_value('username', $row->username),
				'password' => set_value('password', $row->password),
				'role' => set_value('role', $row->role),
			);
			$this->load->view('users/users_form', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('users'));
		}
	}

	public function update_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id', TRUE));
		} else {
			$data = array(
				'pegawai_id' => $this->input->post('pegawai_id',TRUE),
				'username' => $this->input->post('username',TRUE),
				'password' => $this->input->post('password',TRUE),
				'role' => $this->input->post('role',TRUE),
			);

			$this->Users_model->update($this->input->post('id', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('users'));
		}
	}

	public function delete($id)
	{
		$row = $this->Users_model->get_by_id($id);

		if ($row) {
			$this->Users_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('users'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('users'));
		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('pegawai_id', 'pegawai id', 'trim|required');
		$this->form_validation->set_rules('username', 'username', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required');
		$this->form_validation->set_rules('role', 'role', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('', '');
	}

}

/* End of file Users.php */
/* Location: ./application/controllers/Users.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-12-15 16:27:58 */
/* http://harviacode.com */