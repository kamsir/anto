<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('encryption');
		$this->load->library('encrypt');
		$this->load->library('form_validation');
		$this->load->model('Auth_model');
	}

	public function logged_in_check()
	{
		if ($this->session->userdata("logged_in")) {
			redirect("main");
		}
	}

	public function index()
	{
		$this->logged_in_check();
		$data = array(
			'action' => site_url('auth/login'),
		);
		$this->load->view('login',$data);
	}

	public function login()
	{
		$this->_rules();
		if ($this->form_validation->run() == FALSE) {
			$this->index();
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$login = $this->Auth_model->validate($username, $password);
			if ($login == FALSE) {
				$this->index();
				$this->session->unset_userdata('error');
			}
			else{
				$this->session->set_userdata($login);
				$this->session->set_userdata("logged_in", true);
				redirect('main','refresh');
			}
		}
	}

	public function logout()
	{
		$this->session->unset_userdata("logged_in");
		$this->session->sess_destroy();
		redirect("auth",'refresh');
	}

	public function _rules()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function register()
	{
		$data = array(
			'action'=>site_url('auth/register_action')
		);
		$this->load->view('register',$data);
	}

	public function register_action()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[users.username]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			$this->register();
		}
		else {
			redirect('main','refresh');
			// $username = $this->input->post('username');
			// $password = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
			// $data = array(
			// 	'username' => $username,
			// 	'password' => $password
			// );
			// $this->Auth_model->insert($data);
			// redirect('auth','refresh');
		}
	}
}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */