<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	protected $access = array('adm','opr');

	function __construct()
	{
		parent::__construct();
		$this->load->model('Scraping_model');
		$this->load->library('Simple_html_dom');
	}

	public function index()
	{
		$url = "http://cjr.catenzo.co.id/stok.php?id=35&amp;kode=%s";
		// print_r(substr($url, 3, strpos($url, ".ca")));
		// print_r(strpos($url, ".ca"));
		$no_http = substr($url, 7);
		// print_r($no_http);
		// echo "<br>";
		// $a = substr($no_http, 0, strpos($no_http, '/'));
		// print_r($a);
		// echo "<br>";
		// print_r(substr($url, 7, strpos($url, '.')));
		$data_ket = $this->Scraping_model->get_ket();
		$data_stok = $this->Scraping_model->get_stok();
		$data_scrap = $this->Scraping_model->get_scrap();
		$data = array(
			'page_name' => 'scraping/index',
			'action'=> site_url('main/process'),
			'data_ket' => $data_ket,
			'data_stok' => $data_stok,
			'data_scrap' => $data_scrap,
		);
		$this->load->view('index',$data);

	}

	public function scraping($arr_url,$page,$user_id)
	{
		print_r($arr_url);

		foreach ($arr_url as $i => $url) {
			$html = file_get_html($url);
			$max = $i+++1;
			if ($max > $page) {
				break;
			}
			//FIND URL PRODUCT
			foreach ($html->find('div[id=result]') as $data) {
				$table_keterangan = [];
				$table_stok = [];
				$insert = array();

				//TABLE KETERANGAN
				unset($insert);
				foreach ($data->find('div[id=fr-ket-gbr]') as $key => $value) {
					foreach ($value->find('table') as $key => $value) {
						$field = array();
						$label = array();
						foreach ($value->find('tr') as $row) {
							$field[] = $row->find('td',2)->innertext;
							$label[] = $row->find('td',0)->innertext;
						}
						$insert[]= array(
							'no'=>$field[0],
							'kode'=>$field[1],
							'warna' => $field[2],
							'berat' => $field[3],
							'bahan' => $field[4],
							'info' => $field[5],
							'scraping_id'=>$user_id
						);
						$table_keterangan = $insert;
					}
				}
			}
			$this->Scraping_model->insert($insert);

			//TABLE STOK
			$i = 0;
			$kode = array();
			foreach ($data->find('div[id=fr-stok]') as $key => $value) {
				foreach ($value->find('table') as $key => $value) {
					$stok = array();
					$id = array();
					foreach ($value->find('tr') as $no =>$row) {
						$td = array();
						foreach ($row->find('td') as $key => $value) {
							$td[] = $value->plaintext;
						}
						if (isset($td[0])) {
							foreach ($insert as $key => $value) {
								$kode[] = $value['kode'];
							}
							$stok[]= array(
								'size'=>$td[0],
								'stok'=>$td[1],
								'pesanan' => $td[2],
								'last_update' => $td[3],
								'scraping_id'=>$user_id
							);
						}
					}
					if (count($stok) % 5 === 0) {
							// $stok[0] = $kode[$i++];
							// $result = array_push($stok['id'],$kode[$i++]);
					}
				}
				$table_stok[] = array_filter($stok);
			}
			foreach ($insert as $val) {
				$id[] = $val['kode'];
			}
			$this->Scraping_model->insert_stok($id,$table_stok);
				// echo "<pre>";
				// print_r($id);
				// print_r($table_stok);
				// echo "</pre>";
		}
		redirect(site_url('main'));
	}


	public function process()
	{
		$tmp = array();
		$url = $this->input->post('url');
		$tmp[]= $url;
		$nextLink = $url;
		while ($nextLink) {
			$next ='';
			$html = file_get_html($nextLink);
			foreach ($html->find('a') as $data) {
				if ($data->innertext == "Next") {
					$next = $data->href;
					$link = substr($url, 0 ,strpos($url, ".id/")).".id/".$next;
				}
			}
			$nextLink = ($next ? $link : NULL);
			$tmp[] = $nextLink;

			$html->clear();
			unset($html);
		}
		$no_http = substr($url, 7);
		$product = substr($no_http, 0, strpos($no_http, '/'));
		$user = array(
			'users_id' => $this->session->userdata('id'),
			'product' => $product,
			'note' => $this->input->post('note'),
		);
		// print_r($user);
		$user_id = $this->Scraping_model->insert_scraping($user);
		// $user_id = $this->Scraping_model->get_user_id();
		$this->scraping(array_filter($tmp),$this->input->post('page'),$user_id);
		// strip_tags
	}
}
/* End of file Index.php */
/* Location: ./application/controllers/Index.php */